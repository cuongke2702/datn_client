import React from 'react';
import Cart from './components/Cart/Cart';
import CheckOut from './components/CheckOut/CheckOut';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import HomePage from './pages/HomePage/HomePage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage'
import ReviewProductPage from './pages/ReviewProductPage/ReviewProductPage';


const routes = [
    {
        path: '/home',
        exact: true,
        main: () => <HomePage />
    },
 
    {
        path: '/cart',
        exact: true,
        main: () => <Cart/>
    },
    {
        path: '/checkout/:cartId',
        exact: true,
        main: (match) => <CheckOut match={match}/>
    },
    {
        path: '/product-detail/:productId',
        exact: false,
        main: ({match, history}) => <ReviewProductPage match={match} history={history}/>
    },
    {
        path: '/auth/login',
        exact: false,
        main: () => <Login/>
    },
    {
        path: '/auth/register',
        exact: false,
        main: () => <Register/>
    },
   
    {
        path: '',
        exact: false,
        main: () => <NotFoundPage />
       
    }
];

export default routes;