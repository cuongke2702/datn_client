import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actGetProductsRequest,actGetComment, addCommentFetchAPI,addCat } from '../../actions';
import jwt_decode from "jwt-decode";

class ReviewProductPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          productId: "",
          productName: "",
          images: "",
          price: "",
          quantity: "",
          detail : "",
          userId:'',
          cartId:'',
          comments:[],
          comment: {
            context:'',
            productId:'',
            user_Id:'',
            username:'',
          },
          product:{
            cartId:"",
            userId:"",
            productId: "",
            productName: "",
            quantity: "",
            price: "",
          }
        };
      }
      componentDidMount() {
        var { match } = this.props;
        if (match) {
          var productId = match.params.productId;
          this.props.onGetProduct(productId);
          this.props.onGetComment(productId);
        }
        const {isLoggedIn}=this.props
        var decoded=null;
        var userId=null;
        if(isLoggedIn!=null){
          const {user}=isLoggedIn;
          decoded=jwt_decode(user.token);
          userId=decoded.id;
          console.log(userId)
        }
        this.setState({
          userId:userId
        })
        

      }
      componentDidUpdate(prevProps, prevState) {
        if (prevProps.itemEditing !== this.props.itemEditing) {
          const { itemEditing } = this.props;
          this.setState({
            productId: itemEditing.productId,
            productName: itemEditing.productName,
            images: itemEditing.images,
            price: itemEditing.price,
            quantity: itemEditing.quantity,
            detail: itemEditing.detail,
            product:{
              productId:itemEditing.productId,
              productName: itemEditing.productName,
              price: itemEditing.price,
            }
          });
        }
        if(prevProps.comments !== this.props.comments){
          const {comments}=this.props
          this.setState({
            comments:comments
          })
        }
      }

      addComment = (event) =>{
        event.preventDefault();
        const user = JSON.parse(localStorage.getItem("user"));
        if(user!==null){
        var context=event.target.context.value;
         const comment = {
            context:context,
            productId:this.state.productId,
            user_Id:'1',
         }
         this.props.onaddComment(comment,user.token);
         event.target.context.value="";
       }else {
        alert('ban phai login truoc khi dang nhap');
        this.props.history.push('/auth/login');
       }
      };
 
       handleChange = (event) => {
         const target = event.target;
         const value = target.value;
         const name = target.name;
         this.setState({
           [name]: value,
         });
       };

       handleChangess = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
          product:{
            productId:this.state.productId,
            productName: this.state.productName,
            price: this.state.price,
            quantity:value,
            userId:this.state.userId
          }
        });
      };

    render() {
        var { productName,images,price,quantity,detail,product} = this.state;
        var {comments} = this.state;
        const cart = JSON.parse(localStorage.getItem("cart"));
        
        return (
            <div>
            {/* thông tin sản phẩm */}
      <div className="container-detail-product">
        <form method="get" action="checkout.html">
          <div className="detail-product mx-auto">
            <div className="img-product-review col-lg-4 col-md-12 col-sm-12 float-left">
             
              {/* <div className="img-selected mx-auto" style={{backgroundImage: 'url(images/dau-rung.jpg)'}}>
              </div> */}

              <img className="img-selected mx-auto" src={`../../`+images} alt=""/>
            
            
              <div className="other-img">
                <ul>
                  <li className="img-clicked">
                    <div className="item" style={{backgroundImage: 'url(../../images/dau-rung.jpg)'}}>
                    </div>
                  </li>
                  <li>
                    <div className="item" style={{backgroundImage: 'url(../../images/Chuoi.jpg)'}}>
                    </div>
                  </li>
                  <li>
                    <div className="item" style={{backgroundImage: 'url(../../images/tao-do.jpg)'}}>
                    </div>
                  </li>
                  <li>
                    <div className="item" style={{backgroundImage: 'url(../../images/na-đài-loan.jpg)'}}>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="infor-product-review col-lg-8 col-md-12 col-sm-12 float-left">
              <div className="content-infor-product">
                {/* dòng hiển thị tiêu đề , tên sản phẩm , giới thiệu ..... */}
                <div className="describe-product">
                  <h3>{productName}</h3>
                </div>
                {/* dòng hiển thị thông tin đánh giá sản phẩm */}
                <div className="vote-star" style={{width: '95%', height: '25px'}}>
                  <div className="stars">
                    <span className="float-left" style={{color: '#53a6eb', marginRight: '20px'}}>Đánh giá:</span>
                    <span>
                      <ul>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star" />
                        </li>
                      </ul>
                    </span>
                    <span className="total-vote float-left"><h6>({quantity})</h6></span>
                    <span className="float-right">
                      <span>
                        <button className="share-btn btn-normal" type="button" name="share-btn" title="chia sẻ">
                          <i className="fa fa-share" />
                        </button> 
                      </span>
                      <span>
                        <button className="vote-star-btn btn-normal" type="button" name="vote-star-btn" title="đánh giá">
                          <i className="fa fa-heart" />
                        </button>
                      </span>
                    </span>
                  </div>
                </div>
                {/* dong hiển thị thông tin thương hiệu */}
                <div className="trademark-product float-left" style={{width: '95%', height: '50px', borderBottom: '1px solid orange'}}>
                  <span style={{marginRight: '10px', color: '#53a6eb'}}>Thương hiệu: </span>
                  <span style={{borderRight: '1px solid orange', paddingRight: '10px'}}><a href>olala</a></span>
                  <span style={{marginLeft: '10px'}}><a href>{detail}</a></span> 
                </div>
                {/* dòng hiển thị giá sản phẩm */}
                <div className="price-product">
                  <h1 style={{color: 'orange'}}>{price} <span style={{textDecorationLine: 'underline', marginLeft: '2px', fontSize: '90%'}}>đ</span></h1>
                </div>
                {/* dòng hiển thị giá cũ và số tiền tiết kiệm dc */}
                <div className="sale-price" style={{marginBottom: '25px'}}>
                  <h6><span style={{marginRight: '10px', color: 'gray'}}><strike>{price}</strike></span><span style={{textDecoration: 'underline', fontSize: '90%', color: 'gray'}}>đ</span><span style={{marginLeft: '10px'}}>-10%</span> </h6>
                </div>
                {/* dòng hiển thị ưu đãi khi mua sản phẩm */}
                <div className="uu-dai" style={{width: '95%', height: '40px', borderBottom: '1px solid orange'}}>
                  <span className="float-left" style={{marginRight: '20px', lineHeight: '30px', color: 'gray'}}>Ưu đãi</span>
                  <div className=" float-left" style={{width: '70%'}}>
                    <div className="bar-uu-dai-piece-1">
                    </div>
                    <div className="bar-uu-dai-piece-2">
                      giảm 80.000 cho đơn hàng trên 300.000
                    </div>
                  </div>
                </div>
                {/* dòng hiển thị nhóm mã màu */}
                <div className="bar-code-colors" style={{marginTop: '20px'}}>
                  <span className="float-left" style={{paddingRight: '20px', color: 'gray'}}>Mã màu</span>
                  <div className="list-code-color">
                    <ul>
                      <li>
                        <div className="cell-code-color" style={{width: '30px', height: '30px', marginRight: '10px', backgroundColor: 'orange', border: '1px solid blue'}}>
                        </div>
                      </li>
                      <li>
                        <div className="cell-code-color" style={{width: '30px', height: '30px', marginRight: '10px', backgroundColor: 'blue', border: '1px solid blue'}}>
                        </div>
                      </li>
                      <li>
                        <div className="cell-code-color" style={{width: '30px', height: '30px', marginRight: '10px', backgroundColor: 'green', border: '1px solid blue'}}>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                {/* dòng hiển thị tùy chọn kính thước */}
                <div className="bar-size float-left" style={{marginTop: '20px'}}>
                  <span className="float-left" style={{paddingRight: '20px', color: 'gray'}}>Kích cỡ</span>
                  <div className="list-size">
                    <ul>  {/* danh sách kích thước sản phẩm */}
                      <li>
                        <div className="cell-size" style={{width: '40px', height: '30px', marginRight: '10px', border: '1px solid orange'}}>
                          M
                        </div>
                      </li>
                      <li>
                        <div className="cell-size" style={{width: '40px', height: '30px', marginRight: '10px', border: '1px solid orange'}}>
                          ML
                        </div>
                      </li>
                      <li>
                        <div className="cell-size" style={{width: '40px', height: '30px', marginRight: '10px', border: '1px solid orange'}}>
                          XL
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                {/* dòng hiển thị thanh tùy chọn số lượng và kích cỡ màu sắc */}
                <div className="bar-option-num-product float-left" style={{width: '100%', height: '40px', lineHeight: '40px'}}>
                  <span>
                    <span style={{marginRight: '20px', color: 'gray'}}>Số lượng</span>
                    {/* <span>
                      <button type="button" onClick={this.DESC} id="btn-sub-num-p" style={{cursor: 'pointer', color: 'gray'}}><i className="fa fa-minus" style={{fontSize: '150%'}} /></button>
                    </span> */}
                    <input type="text" name="quantitygiohang" defaultValue={this.state.product.quantity}  onChange={this.handleChangess} className="num-p"  id="quantitygiohang" style={{color: '#095764', fontSize: '150%', width: '30px', textAlign: 'center', fontWeight: 'bold'}}/>
                    {/* <span>
                      <button type="button" onClick={this.ASC} id="btn-add-num-p" style={{cursor: 'pointer'}}><i className="fa fa-plus" style={{fontSize: '150%'}} /></button>
                    </span> */}
                  </span>
                </div>	
                {/* dòng hiển thị nút thêm vào giỏ hàng và mua hàng */}
                <div className="btn-add-and-buy float-left" style={{width: '100%', height: '50px'}}>
                  <span>
                    <button type="button"  className="btn-add-into-cart btn-large-size" style={{height: '100%', backgroundColor: 'darkorange', color: 'white', borderRadius: '5px'}}
                    onClick={ () => this.onAddToCart(this.state.product) }  >THÊM VÀO GIỎ HÀNG</button>
                  </span>
                  {/* phần thông báo đã thêm sản phẩm vào giỏ hàng  */}
                  <span>
                    <button type="submit"  className="btn-buy-now btn-large-size" style={{height: '100%', backgroundColor: 'orange', color: 'white', borderRadius: '5px'}}>MUA NGAY</button>
                  </span>
                </div>	
              </div>
            </div>
          </div>
        </form>		
      </div>
      <div className="container-description col-lg-10 col-md-10 col-sm-12 col-12">
      <div className="description-content ">
        <ul className=" ul-item mx-auto">
          <li className="li-item">
            <div className="tab-description  show-tab">
              <div className="comment-product">
               
              <div className="comment-content">
                  <div className="question-about-product">
                    <div>
                      <h3>Phần hỏi đáp về sản phẩm</h3>
                    </div>
                    <div className="area-input-question">
                    <form onSubmit={this.addComment}>
                      <table style={{width: '100%', height: '100%'}}>
                        <tbody><tr>
                            <td style={{width: '90%', height: '100%'}}>
                              <span className="input-txt">
                                <input type="text" name="context" placeholder="Nhập câu hỏi tại đây" />
                              </span>
                            </td>
                            <td style={{width: '10%', height: '100%'}}>
                              <span className="btn-send">
                                <button type="submit" ><i className="fa fa-paper-plane" /></button>
                              </span>
                            </td>
                          </tr>
                        </tbody></table>	
                        </form>
                    </div>
                  
                    <div  className="list-question" style={{width: '100%'}}>
                      <ul>
                      {comments? comments.map(comment =>(
                        <li key={comment.commentId} className="question-item">
                          <div className="question">
                            <div className="infor-user">
                              <span className="icon"><i className="fa fa-comment" /></span>
                              <span className="user-name">Trần Ngọc Hiếu</span>
                              <span className="action">{comment?.createdAt}</span>
                            </div>
                            <div className="content" id="content">
                             {comment.context}
                            </div>	
                          </div>
                        </li>
                        )) : <>gfhgchgchg</>
                      }
                      </ul>
                    </div>
                  </div>

                </div>
              
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    
            </div>
            
        );
    }
    onAddToCart = (product) => {
      console.log(product)
      const user = JSON.parse(localStorage.getItem("user"));
      const cart = JSON.parse(localStorage.getItem("cart"));
        var cartId=0;
        if(cart!==null){
          cartId=tudongtangCart(cart.length)
        }
        var products={
          cartId:cartId,
          productId:product.productId,
          productName: product.productName,
          price: product.price,
          quantity:product.quantity,
          userId:product.userId
        }

      if(user!==null){
      this.props.onAddCartFect(products);
    }else {
      alert('ban phai login truoc khi dang nhap');
      this.props.history.push('/auth/login');
    }
  }
}

const mapStateToProps = (state) => {
    return {
      itemEditing: state.itemEditing,
      comments: state.comments,
      isLoggedIn : state.users
    };
  };
  const mapDispatchToProps = (dispatch, props) => {
    return {
      onGetProduct: (productId) => {
        dispatch(actGetProductsRequest(productId));
      },
      onGetComment: (productId) =>{
        dispatch(actGetComment(productId))
      },
      onaddComment: (comment,token) =>{
        dispatch(addCommentFetchAPI(comment,token))
      },
      onAddCartFect : (product) =>{
        dispatch(addCat(product))
      }
      
    };
  };

  var tudongtangCart = (cartId) => {
    var cartNumber = 0;
    cartNumber=cartId+1;
    return cartNumber;
  };

export default  connect(mapStateToProps, mapDispatchToProps)(ReviewProductPage);
