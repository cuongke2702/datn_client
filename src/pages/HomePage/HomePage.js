import React, { Component } from "react";
import ProductCenter from "../../components/ProductItem/ProductCenter";
import ProductItem from "../../components/ProductItem/ProductItem";
import ProductSale from "../../components/ProductItem/ProductSale";
import ProductStore from "../../components/ProductItem/ProductStore";


class HomePage extends Component {
  render() {
    return (
      <div>
         <div style={{ textAlign: "center" }}>
        <ProductItem />

        <ProductCenter />

        <ProductSale />

        <ProductStore />
        </div>
      </div>
    );
  }
}

export default HomePage;
