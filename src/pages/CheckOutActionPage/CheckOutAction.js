import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addThanhToan } from '../../actions';



class CheckOutAction extends Component {

  constructor(props) {
    super(props);
    this.state = {
    product:{},
    };
  }

  componentDidMount(){
    const {cart, match}=this.props;
   
    let product=null;
    if (match) {
      const {cartId} = match.match.params;
      product= cart.find(each => each.cartId.toString() === cartId.toString())
      this.setState({
        product:product,
      })
    }
  }

  thanhtoan12=(product)=> {
    console.log('sadasd');
    this.props.onthanhtoan(product);
  }


    render() {
      const {product}=this.state
      console.log(product);
        return (
        <div>
          <div className="col-right col-lg-4 col-md-4 col-12">
          <div className="content">
            <div className="bar-to-pay-btn ">
              <button type="button" className="bg-button-enable">THANH TOÁN</button>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Tạm tính</div><div className="value" id="price-id-add-1">120.000</div>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Phí giao hàng</div><div className="value" id="price-id-add-2" />
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Giảm phí giao hàng</div><div className="value" id="price-id-sub-1"><span>-</span>5.000</div>
            </div>	
            <div className="row-checkout-of-col-right row-checkout-of-col-right-show">
              <div className="input-discount-code">
                <input type="text" name="txt-discount-code" defaultValue placeholder="Nhập mã giảm giá" />
              </div>
              <div className="btn-apply-code">
                <button type="button">ÁP DỤNG</button>
              </div>
            </div>
            <div className="row-checkout-of-col-right row-checkout-of-col-right-hide">
              <div className="row-label">Giảm giá</div><div className="value" id="price-id-sub-2"><span>-</span>20.000</div>
            </div>
            <div className="row-checkout-of-col-right">
              <div className="row-label">Tổng</div><div className="value" id="summany" />
            </div>	
            <div className="bar-to-pay-btn ">
              <button type="button" onClick={()=>this.thanhtoan12(product)} className="bg-button-enable">THANH TOÁN</button>
            </div>
          </div>
        </div>	
     
            </div>
        );
    }
}

// const mapStateToProps = (state) => {
//   return {
//     isLoggedIn : state.users,
//     cart:state.cart,
//   };
// };
const mapDispatchToProps = (dispatch, props) => {
  return {
    onthanhtoan: (product) => {
      dispatch(addThanhToan(product));
    },
  };
};

export default connect(null,mapDispatchToProps)(CheckOutAction);