import callApiHeder from '../utils/callAPIHeaders';
import * as Types from './../constants/ActionTypes';
import callApi from './../utils/apiCaller';
import { Redirect } from 'react-router-dom';
import jwt_decode from "jwt-decode";


//FETCH_DATA ----CALL API (Find_All)
export const actFetchProductsRequest = () => {
    return async (dispatch) => {
        const res = await callApi('api/product/getListProduct', 'GET', null);
        dispatch(actFetchProducts(res.data.data));
    };
}

export const actFetchProducts = (products) => {
    return {
        type : Types.FETCH_PRODUCT,
        products
    }
}

//FETCH_DATA ----CALL API (GET_ID_REQUEST)
export const actGetProductsRequest = (productId) =>{
    return async (dispatch) => {
        const res = await callApi(`api/product/findProductById/${productId}`,'GET',null);
        dispatch(actGetProduct(res.data.data));
    }
}

export const actGetProduct = (product) =>{
    return{
        type : Types.GET_PRODUCT,
        product
    }
}


export const actAddToCart = (product, quantity) => {
    return {
        type: Types.ADD_TO_CART,
        product,
        quantity
    }
}


export const getComment = (comments) => {
    return {
        type: Types.GET_COMMENT,
        comments,
    }
}

export const actGetComment = (productId) =>{
    return dispatch =>{
        return callApi(`api/comment/getListComment/${productId}`,'GET',null).then(res =>{
            dispatch(getComment(res.data.data));
        })
    }
}

export const addComment= (comment,userName) =>{
    return{
        type: Types.ADD_COMMENT,
        comment,
        userName
    }

}

export const addCommentFetchAPI = (comment,token) =>{
    return dispatch =>{
        var decoded=jwt_decode(token)
        return callApiHeder(`api/comment/add`,'POST',comment,token).then(res =>{
            dispatch(addComment(res.data.data,decoded.sub));
        })
    }
}

export const addUser= (user) =>{
    return{
        type: Types.REGISTER_USER,
        user,
    }
}

export const addUserFetchAPI = (user) => async dispatch=>{
    try{
        const {data} = await callApi(`api/user/add`,'POST',user);
        console.log(data)
        return dispatch(addUser(data));
    }catch(error){
        console.log(error.response);
    }
}

// export const addStudentRequest2= (student) => async dispatch=>{
//     try {
//         const {data} =await axios.post(`http://localhost:8888/api/student/add`,{
//             studentId :student.studentId,
//             name:student.name,
//             tuoi:student.tuoi,
//             lop:{
//                 lopId:student.lopId,
//             }
//         })
//         return dispatch(addStudent(data))
//     } catch (error) {
//         console.log(error.response);
//     }
// }

export const loginUserSuccess= (user) =>{
    return{
        type: Types.LOGIN,
        user,
    }

}

export const loginUser = (user) =>{
    return dispatch =>{
        return callApi(`auth/login`,'POST',user).then(res =>{
            if(res.data.data){
                localStorage.setItem("user", JSON.stringify(res.data.data));
            }
            dispatch(loginUserSuccess(res.data.data));
        }).catch(error =>{
            console.log(error.response)
        })
    }
}

export const logoutUser= () =>{
    return{
        type: Types.LOGOUT,
    }

}

export const logouUserFectAPI = () =>{
    return dispatch =>{
        localStorage.removeItem("user");
        localStorage.removeItem("cart");
        dispatch(logoutUser)
    }
}

export const addCartAction= (product) =>{
    return{
        type: Types.ADD_TO_CART_ITEM,
        product,
    }
}

export const addCat =(product) =>{
    return dispatch =>{
        localStorage.setItem("cart", JSON.stringify(product));
        dispatch(addCartAction(product))
    }
}

export const getUser=(user)=>{
    return {
        type: Types.GET_USER,
        user,
    }
}


export const getUserById = (userId) => {
    return async (dispatch) => {
        const res = await callApi(`api/user/getuser/${userId}`, 'GET', null);
        dispatch(getUser(res.data.data));
    };
}

export const getProductCart=(cartId)=>{
    return {
        type: Types.GET_PRODUCT_CART,
        cartId,
    }
}

export const getPoductinCart = (cartId) =>{
    return dispatch =>{
     dispatch(getProductCart(cartId))
    }
}

export const thanhtoan=(product)=>{
    return {
        type: Types.THANH_TOAN,
        product,
    }
}

export const addThanhToan = (product) =>{
    let  products={
        productId:product.productId,
        quantity:product.quantity,
        product:product.price,
        userId:product.userId
    }
    return dispatch =>{
        return callApi(`api/order/add`,'POST',products).then(res =>{
            console.log(res.data.data);
            dispatch(thanhtoan(product));
        }).catch(error =>{
            console.log(error.response)
        })
    }
}



