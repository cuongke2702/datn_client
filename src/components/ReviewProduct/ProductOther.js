import React, { Component } from 'react';

class ProductOther extends Component {
    render() {
        return (
            <div>
                   {/* sản phẩm liên quan */}
      <div className="list-product-result">
        <div className="title">
          <h3>Sản phẩm liên quan</h3>
        </div>
        <div className="content mx-auto">
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Chuối và hương</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/dau-rung.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Dâu rừng</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/tao-do.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Táo đỏ</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/na-đài-loan.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Na rừng</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Chuối và hương</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li><li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
          <a href="review-product.html" style={{color: 'black'}}>
            <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
              <div className="content-product">
                <div className="img-product-content" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                </div>
                <div className="name-product">
                  <h5>Chuối và hương</h5>
                </div>
                <div className="price-product" style={{color: '#ff8100'}}>
                  <span className="float-left" style={{lineHeight: '40px'}}>đ</span>
                  <h3>25.000</h3>
                  <h6 style={{color: 'gray'}}><strike>30.000</strike> -16%</h6>
                </div>
                <div className="stars">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                  <div className="total-vote"><h6>(71)</h6></div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            </div>
        );
    }
}

export default ProductOther;