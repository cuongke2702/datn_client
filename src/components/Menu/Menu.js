import React, { Component } from "react";
import { Link } from "react-router-dom";
import jwt_decode from "jwt-decode";
import Loginfile from "./loginfile";
import { connect } from "react-redux";
import { logouUserFectAPI } from '../../actions';

class Menu extends Component {

  constructor(props) {
    super(props);
    this.state = {
     numCarts:'',
     isLoggedIn:{},
    };
  }


  componentDidUpdate(prevProps, prevState) {
    if (prevProps.cart !== this.props.cart) {
      const { cart } = this.props;
      this.setState({
        numCarts: cart.length
      });
    }
    if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
      const { isLoggedIn } = this.props;
      this.setState({
        isLoggedIn:isLoggedIn
      });
    }
  }

  logout=()=>{
    this.props.onLogout();
  }
  render() {
    const user = JSON.parse(localStorage.getItem("user"));
    const {isLoggedIn}=this.state
    var decoded= null;
   if(user!==null){
    decoded = jwt_decode(user.token);
   }
   const {numCarts}=this.state
    return (
      <div>
        <div className="header">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <button
              className="navbar-toggler "
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
              style={{ outline: "none", border: "none" }}
            >
              <span className="menu-icon">
                <i className="fa fa-bars" />
              </span>
            </button>
            <div
              className="collapse navbar-collapse "
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav  mx-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="#">
                    Chăm sóc khách hàng
                    <span className="sr-only">(current)</span>
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="register-sell.html">
                    Bán hàng
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Kiểm tra đơn hàng
                  </a>
                </li>
                {
                  (decoded!==null)?
                  <div>
                   <div>{decoded.sub}</div>
                   <Link to={`/auth/login`}>
                   <li className="nav-item">
                     <a className="nav-link"  onClick={this.logout}  >
                       Đăng xuất
                     </a>
                   </li>
                   </Link>
                   </div>
                   :
                  <Loginfile/>
                }
                 
                <li
                  className="nav-item"
                  style={{
                    visibility: "hidden",
                    opacity: 0,
                    zIndex: 1000,
                    position: "fixed",
                  }}
                >
                  <a className="nav-link" href="#">
                    Thông tin cá nhân
                  </a>
                </li>
              </ul>
            </div>
          </nav>
          <div className="bar-search">
            <div className="logo" id="id-logo">
              <a href="#" className="home-link" title="Trang chủ">
                <img src="images/logo_vungcaofood.png" width="100%" />{" "}
              </a>
            </div>
            <div className="search-blank  ">
              <div className="child-search-blank mx-auto ">
                <input
                  className="form-search"
                  type="search"
                  placeholder="Tìm kiếm trên VungCaoFood..."
                />
                <button className="btn-search " type="submit" title="Tìm kiếm">
                  <i className="fa fa-search" />
                </button>
              </div>
            </div>
            <span className="cart-box">
              <Link to={'/cart'}>
              <a href="cart.html">
                <span className="cart" title="Giỏ hàng">
                  <i className="fa fa-shopping-cart" />
                </span>
                <span className="num-cart">{numCarts? numCarts:0}</span>
              </a>
              </Link>
            </span>
          </div>
          <div className="mini-menu">
            <div className="btn-list-product">
              <span className="title-of-list-product">Danh mục</span>
              <span className="icon-of-list-product">
                <i className="fa fa-angle-down" />
              </span>
            </div>
            <div className="mini-sevice-content ">
              <div className="content mx-auto">
                <div className="mini-card-service" title="Liên hệ">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-shopping-bag" />
                    </span>
                    <span className="title-card">Hàng chính hãng</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Đánh giá">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-star" />
                    </span>
                    <span className="title-card">Đánh giá cao</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Mã giảm giá">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-bookmark" />
                    </span>
                    <span className="title-card">Mã giảm giá</span>
                  </a>
                </div>
                <div className="mini-card-service" title="Ưu đãi">
                  <a href="#">
                    <span className="card-icon">
                      <i className="fa fa-heart" />
                    </span>
                    <span className="title-card">Ưu đãi</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
    
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn : state.users,
    cart:state.cart,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
      onLogout : () =>{
          dispatch(logouUserFectAPI())
      }
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(Menu);
