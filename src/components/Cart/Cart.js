import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import CartActionUpdate from '../../pages/CartActionPage/CartActionUpdate';

class Cart extends Component {

  constructor(props) {
    super(props);
    this.setState({
      Numcart:'',
    })
  }

  componentDidMount(){

  }
    render() {
      const {cart}=this.props
     // const {cart} =this.state
      //console.log(this.state)
      return (
        <div className="list-product-of-cart">
        <div className="list-content mx-auto">
          <div className="title"><h5>GIỎ HÀNG</h5></div>
          <ul>
            {
              cart? cart.map((product, index) =>(
          <li key={index} id={1+1}>
        <div className="item-cart" idproductincart={3}>
          <div className="infor-mall">
            <div className="skin">
              <div className="name-of-mall">olala shop</div>
            </div>
            <div className="btn-delete-product-from-cart">
              <button title="xóa sản phẩm"><i className="fa fa-trash" /></button>
            </div>
          </div>
          <div className="img-product" style={{backgroundImage: 'url(images/Chuoi.jpg)'}} urlimage="images/tao-do.jpg">
          </div>
          <div className="quick-infor">
            <div className="name">{}</div>
            <div className="code-color-selected">
              <span className="label">Tên sản phẩm:</span><span className="value" idcolor="123abc">{product.productName}</span>
            </div>
            <div className="size-selected">
              <span className="label">Giá:</span><span className="value" idsize="10 lít">{product.price}</span>
            </div>
            <div className="qty-selected">
              <span className="label">Số lượng:</span><span className="value" qtyproduct={1}>{product.quantity}</span>
            </div>
            <div className="bar-button-feedback">
              <span>
                <button>
                  <i className="fa fa-share" />
                </button>
              </span>
              <span>
                <button>
                  <i className="fa fa-heart" />
                </button>
              </span>
            </div>
          </div>
          <div className="price-product">
            <div className="price">
              <span>{product.quantity*product.price}</span><span style={{textDecorationLine: 'underline'}}>đ</span>
            </div>
          </div>
          <div className="col-btn-right">
            <div className="btn-change-option-product">
              <button>CHỈNH SỬA</button>
            </div>
            <div className="btn-buy-product">
                <Link to={`/checkout/${product.cartId}`}><button>THANH TOÁN</button></Link>
            </div>
          </div>
        </div>	
      </li>
       )) : <>gfhgchgchg</>
       }
           <CartActionUpdate/>
           
          </ul>
        </div>
      </div>
            
      );      
  }
}
const mapStateToProps = state => {
  return {
      cart: state.cart
  }
}

export default connect(mapStateToProps, null)(Cart);