import React, { Component } from 'react';
import Flickity from 'react-flickity-component';
import "./style.css"


const flickityOptions = {
  initialIndex: 0
}

const PopularCarrousel = () => {
  return (
    <Flickity
      className={'carousel'}
      elementType={'div'} // default 'div'
      // options={flickityOptions} // takes flickity options {}
      disableImagesLoaded={false} // default false
      reloadOnUpdate
      static // 
    >
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
      <a href="#">
        <div className="most-popular-product">
          <div className="most-popular-product-content">
            <div className="img-product-content" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center' }}>
              <h5>Hoa quả</h5>
            </div>
            <div className="name-popular-product" style={{ textAlign: 'center', color: 'gray' }}>
              <h6>1.240 sản phẩm</h6>
            </div>
          </div>
        </div>
      </a>
    </Flickity>
  )
}

export default PopularCarrousel;