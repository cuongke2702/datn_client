import React, { Component } from 'react';
import Flickity from 'react-flickity-component';
import ProductItem from '../ProductItem/ProductItem';
import "./style.css"


const flickityOptions = {
  initialIndex: 1
}

const ShopCarrousel = () => {
  return (
    <Flickity
      className={'carousel'}
      elementType={'div'} // default 'div'
      // options={flickityOptions} // takes flickity options {}
      disableImagesLoaded={false} // default false
      reloadOnUpdate
      static // 
    >
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div status="hidden" className="mall-carousel-item active">
        <a href="mall.html">
          <div className="tag-vcf-mall">
            <div className="tag-vcf-mall-content">
              <div className="logo-vcf-mall mx-auto">
                <div className="img-logo" style={{ backgroundImage: 'url(images/vcf.jpg)' }}>
                </div>
              </div>
              <div className="name-vcf-mall">
                Olala shop
                    </div>
              <div className="num-type-product-in-mall">
                <span className="num">1.000</span><span style={{ marginLeft: '5px' }}>sản phẩm</span>
              </div>
              <div className="vote-star">
                <div className="stars mx-auto">
                  <ul>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star star-select" />
                    </li>
                    <li>
                      <i className="list-star-item fa fa-star" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </Flickity>
  )
}

export default ShopCarrousel;