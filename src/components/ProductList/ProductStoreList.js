import React, { Component } from "react";
import { connect } from "react-redux";
import { actAddToCart, actFetchProductsRequest } from "./../../actions/index";
import ProductItem from "./ProductItem";

class ProductStoreList extends Component {
  componentDidMount() {
    this.props.fetchAllProducts();
  }
  render() {
    var { products } = this.props;
    return <div>{this.showStaffs(products)}</div>;
  }

  showStaffs(products) {
    var { onAddToCart } = this.props;
    var result = null;
    if (products.length > 0) {
      result = products.map((product, index) => {
        return (
          <ProductItem key={index}
           product={product} onAddToCart = {onAddToCart} />
        );
      });
    }
    return result;
  }
  
}

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllProducts: () => {
      dispatch(actFetchProductsRequest());
    },
    onAddToCart: (product) => {
      dispatch(actAddToCart(product, 1));
  }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductStoreList);
