import React, { Component } from 'react';

class ProductList extends Component {
    render() {
        return (
            <div>
             
            <a href="review-product.html" style={{ color: "black" }}>
              <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
                <div className="content-product">
                  <div
                    className="img-product-content"
                    style={{ backgroundImage: "url(images/vcf.jpg)" }}
                  ></div>
                  <div className="name-product">
                    <h5>Chuối và hương</h5>
                  </div>
                  <div className="price-product" style={{ color: "#ff8100" }}>
                
                    <h3>25.000 VNĐ</h3>
                    <h6 style={{ color: "gray" }}>
                      <strike>30.000</strike> -16%
                    </h6>
                  </div>
                  <div className="stars">
                    <ul>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star star-select" />
                      </li>
                      <li>
                        <i className="list-star-item fa fa-star" />
                      </li>
                    </ul>
                    <div className="total-vote">
                      <h6>(71)</h6>
                    </div>
                  </div>
                </div>
              </div>
            </a>
         
             
            </div>
        );
    }
}

export default ProductList;
