import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ProductItem extends Component {
    render() {
        var { product } = this.props;
        return (
            <div>
              <div style={{ color: "black" }}>
                <div className="item-result-product col-lg-2 col-md-3 col-sm-4 col-6">
                <Link to={`/product-detail/${product.productId}`}>
                  <div className="content-product">
                    {/* <div
                      className="img-product-content"
                      style={{ backgroundImage: "url(images/vcf.jpg)" }} >
                      </div> */}
                    <img
                      src={product.images}
                      style={{ width: "150px", height: "140px" }}
                    />
                    <div className="name-product">
                      <h5>{product.productName}</h5>
                    </div>
                    <div className="price-product" style={{ color: "#ff8100" }}>
                      <h5>{product.price} VNĐ</h5>
                      <h6 style={{ color: "gray" }}>
                        <strike>{product.price} VNĐ</strike> -16%
                      </h6>
                    </div>
                    <div className="stars">
                      <ul>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="list-star-item fa fa-star star-select" />
                        </li>
                        <li>
                          <i className="fa fa-star-o" />
                        </li>
                      </ul>
                      <div className="total-vote">
                        <h6>({product.quantity})</h6>
                      </div>
                      
                    </div>
                  </div>
                  </Link>
                  <a 
                    className="btn-floating blue-gradient" 
                    data-toggle="tooltip" data-placement="top" 
                    title=""  onClick = {() => this.onAddToCart(product)}
                    data-original-title="Add to Cart" >
                    <i className="fa fa-shopping-cart"></i>
                </a>
                </div>
                </div>
        
        
            </div>
        );
    }
    onAddToCart = (product) => {
        this.props.onAddToCart(product);
    }

}

export default ProductItem;