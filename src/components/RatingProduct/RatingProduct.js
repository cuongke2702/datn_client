import React, { Component } from 'react';

class RaitingProduct extends Component {
    render() {
        return (
            <div>
                   <div className="comment-content">
                    {/* thanh tiêu đề và lọc đánh giá */}
                    <table border={2} style={{width: '100%', height: '50px', marginBottom: '40px', color: 'orange'}}>
                      <tbody><tr>
                          <th style={{width: '50%'}}>Nhận xét về sản phẩm</th>
                          <th style={{width: '25%'}}>1</th>
                          <th style={{width: '25%'}}>1</th>
                        </tr>
                      </tbody></table>
                    <div className="list-comment">
                      <ul>
                        <li className="item">
                          <div className="line-comment">
                            <div className="stars">
                              <ul>
                                <li>
                                  <i className="fa fa-star star-select" />
                                </li>
                                <li>
                                  <i className="fa fa-star star-select" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                              </ul>
                            </div>
                            <div className="user-comment-info">
                              <span className="who-commented">
                                <span style={{marginRight: '5px'}}>Bởi:</span><span className="user-name" style={{marginRight: '20px', color: 'orange'}}>Trần Ngọc Hiếu</span>
                              </span>
                              <span style={{color: 'green', fontWeight: 'bold'}}>
                                <span>
                                  <i className="fa fa-check-circle" style={{marginRight: '5px'}}>
                                  </i>
                                </span>xác nhận đã nhận hàng
                              </span>
                              <span className="time-comment float-right">
                                <span id="number">11</span>
                                <span style={{marginLeft: '5px'}} id="date-month-year">ngày trước
                                </span>
                              </span>
                            </div>
                            <div className="content">
                              <div className="text">
                                hàng đẹp như hình, giao hàng nhanh cho hẳn 2 sao :V
                              </div>
                              <div className="list-img-comment" style={{display: 'inline-table'}}>
                                <div className="img-comment" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                                </div>
                                <div className="img-comment" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                                </div>
                                <div className="img-comment" style={{backgroundImage: 'url(images/Chuoi.jpg)'}}>
                                </div>
                              </div>
                            </div>
                            <span className="bar-like">
                              <span>
                                <button type="button" id="btn-like"><i className="fa fa-thumbs-up" /></button>
                              </span>
                              <span id="number-like">2</span>
                            </span>
                          </div>
                        </li>
                        <li className="item">
                          <div className="line-comment">
                            <div className="stars">
                              <ul>
                                <li>
                                  <i className="fa fa-star star-select" />
                                </li>
                                <li>
                                  <i className="fa fa-star star-select" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                                <li>
                                  <i className="fa fa-star" />
                                </li>
                              </ul>
                            </div>
                            <div className="user-comment-info">
                              <span className="who-commented">
                                <span style={{marginRight: '5px'}}>Bởi:</span><span className="user-name" style={{marginRight: '20px', color: 'orange'}}>Trần Ngọc Hiếu</span>
                              </span>
                              <span style={{color: 'green', fontWeight: 'bold'}}>
                                <span>
                                  <i className="fa fa-check-circle" style={{marginRight: '5px'}}>
                                  </i>
                                </span>xác nhận đã nhận hàng
                              </span>
                              <span className="time-comment float-right">
                                <span id="number">11</span>
                                <span style={{marginLeft: '5px'}} id="date-month-year">ngày trước
                                </span>
                              </span>
                            </div>
                            <div className="content">
                              <div className="text">
                                hàng đẹp như hình, giao hàng nhanh cho hẳn 2 sao :V
                              </div>
                              <div className="list-img-comment" style={{display: 'inline-table'}}>
                                {/* <div class="img-comment" style="background-image: url(images/Chuoi.jpg); ">
	    												</div>
	    												<div class="img-comment" style="background-image: url(images/Chuoi.jpg); ">
	    												</div>
	    												<div class="img-comment" style="background-image: url(images/Chuoi.jpg); ">
	    												</div> */}
                              </div>
                            </div>
                            <span className="bar-like">
                              <span>
                                <button type="button" id="btn-like"><i className="fa fa-thumbs-up" /></button>
                              </span>
                              <span id="number-like">2</span>
                            </span>
                          </div>
                        </li>
                      </ul>
                    </div>
                   
                    <div className="question-about-product">
                      <div>
                        <h3>Phần hỏi đáp về sản phẩm</h3>
                      </div>
                    
                      <div className="area-input-question">
                        <table style={{width: '100%', height: '100%'}}>
                          <tbody><tr>
                              <td style={{width: '90%', height: '100%'}}>
                                <span className="input-txt">
                                  <input type="text" name="question-txt" placeholder="Nhập câu hỏi tại đây" />
                                </span>
                              </td>
                              <td style={{width: '10%', height: '100%'}}>
                                <span className="btn-send">
                                  <button type="button"><i className="fa fa-paper-plane" /></button>
                                </span>
                              </td>
                            </tr>
                          </tbody></table>	
                      </div>
                    
                      <div className="list-question" style={{width: '100%'}}>
                        <ul>
                          <li className="question-item">
                            <div className="question">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">Trần Ngọc Hiếu</span>
                                <span className="action">đã đặt câu hỏi</span>
                              </div>
                              <div className="content" id="content">
                                có size nào to hơn nữa không shop?
                              </div>	
                            </div>
                            <div className="answer">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">olala shop</span>
                                <span className="action">đã trả lời</span>
                              </div>
                              <div className="content" id="content">
                                size đó là to nhất rồi đó bạn
                              </div>	
                            </div>
                          </li>
                       
                          <li className="question-item">
                            <div className="question">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">Trần Ngọc Hiếu</span>
                                <span className="action">đã đặt câu hỏi</span>
                              </div>
                              <div className="content" id="content">
                                thấy mấy shop khác có nhưng mà đắt quá
                              </div>	
                            </div>
                            <div className="answer">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">olala shop</span>
                                <span className="action">đã trả lời</span>
                              </div>
                              <div className="content" id="content">
                                shop sẽ nhập sản phẩm về sớm ạ, mong bạn thông cảm
                              </div>	
                            </div>
                          </li>
                       
                          <li className="question-item">
                            <div className="question">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">Trần Ngọc Hiếu</span>
                                <span className="action">đã đặt câu hỏi</span>
                              </div>
                              <div className="content" id="content">
                                ok
                              </div>	
                            </div>
                            <div className="answer">
                              <div className="infor-user">
                                <span className="icon"><i className="fa fa-comment" /></span>
                                <span className="user-name">olala shop</span>
                                <span className="action">đã trả lời</span>
                              </div>
                              <div className="content" id="content">
                                cảm ơn bạn đã quan tâm ạ &lt;3
                              </div>	
                            </div>
                          </li>
                        </ul>

                      </div>
                    </div>

                  </div>
            </div>
        );
    }
}

export default RaitingProduct;