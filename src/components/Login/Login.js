import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginUser } from '../../actions';

class Login extends Component {

  constructor(props) {
    super(props);
     this.state={
            username:'',
            password:'',
            isLoggedIn:{}
      };
  }

  componentDidMount(){

  }

  handelsubmit =(event) =>{
    event.preventDefault();
    var user = {
        username: this.state.username,
        password:this.state.password,
    }
    this.props.onLogin(user);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
      const { isLoggedIn } = this.props;
      this.setState({
        isLoggedIn:isLoggedIn
      });
    }
  }


  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  };
    render() {
      const {isLoggedIn}=this.state
      console.log(isLoggedIn)
      if(isLoggedIn.isLoggedIn){
        console.log('logged');
        return <Redirect to="/home"/>;
      }
        return (
          <div id="login">
          <h3 className="text-center text-white pt-5">Login form</h3>
          <div className="container">
            <div id="login-row" className="row justify-content-center align-items-center">
              <div id="login-column" className="col-md-6">
                <div id="login-box" className="col-md-12">
                  <form id="login-form" className="form" onSubmit={this.handelsubmit}>
                    <h3 className="text-center text-info">Login</h3>
                    <div className="form-group">
                      <label htmlFor="username" className="text-info">Username:</label><br />
                      <input type="text" name="username" id="username" onChange={this.handleChange} className="form-control" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="password" className="text-info">Password:</label><br />
                      <input type="text" name="password" id="password" className="form-control" onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                      <input type="submit" name="submit" className="btn btn-info btn-md" defaultValue="submit" />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
          
        )
    }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn : state.users
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
      onLogin : (user) =>{
          dispatch(loginUser(user))
      }
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(Login);
