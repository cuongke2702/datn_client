import React, { Component } from 'react';
import { connect } from 'react-redux';
import CheckOutAction from '../../pages/CheckOutActionPage/CheckOutAction';
import findProduct from '../../utils/findProduct';

class CheckOutDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
    product:{},
    };
  }

  componentDidMount(){
    const {cart, match}=this.props;
    let product=null;
    if (match) {
      const {cartId} = match.match.params;
      console.log(cartId)
      product= cart.find(each => each.cartId.toString() === cartId.toString())
      this.setState({
        product:product,
      })
    }
   
  }
    render() {
      const {product}=this.state;
        return (
            <div>
                 <div className="package">
            <div className="title-packege" style={{backgroundColor: 'orange', marginBottom: '10px', width: '100%', display: 'inline-block'}}>
              <div className="title-package-left float-left" style={{fontSize: '120%', fontWeight: 'bold'}}>
                Thông tin sản phẩm
              </div>
              <div className="title-package-right float-right">
                <span style={{marginRight: '5px'}}>được giao bởi:</span><span style={{fontWeight: 'bold', color: 'white', marginRight: '5px'}}>olala shop</span>
              </div>
            </div>
            <div className="content-package">
              <div className="col-product-left">
                <div className="infor-product">
                  <div className="img-product" style={{backgroundImage: 'url(images/tao-do.jpg)'}} />
                  <div className="infor-product-right">
                    <div className="name">
                     {product?.productName}
                    </div>
                    <div className="list-properties-product">
                    </div>
                    <div className="qty-product">
                      <span style={{marginRight: '10px, margin-bottom: 20px', marginTop: '20px'}}>số lượng: </span><span style={{color: 'orange', fontWeight: 'bold'}}>{product?.quantity}</span>
                    </div>
                    <div className="icon-ship">
                      <i className="fa fa-car" />
                    </div>
                  </div>
                </div>
                <div className="select-ship">
                  <label style={{width: '100%', paddingLeft: '5px'}}>Lựa chọn giao hàng</label>
                  <div className="option-ship selected-option-ship" data={1}>
                    <div className="ship-fee">
                      <span className="check-box" style={{textAlign: 'center', marginRight: '5px'}}><i className="fa fa-check-circle" style={{color: 'orange'}} /></span><span className="value">12.000</span><span style={{textDecorationLine: 'underline'}}>đ</span>
                    </div>
                    <div className="type-ship" style={{marginLeft: '17px', color: 'orange'}}>Tiêu chuẩn</div>
                    <div className="time-ship" style={{marginLeft: '17px', color: '#00a4bf'}}>Giao hàng trong vòng 7 ngày</div>
                  </div>
                  <div className="option-ship" data={0}>
                    <div className="ship-fee">
                      <span className="check-box" style={{textAlign: 'center', marginRight: '5px'}}><i className="fa fa-circle" style={{color: 'orange'}} />
                      </span>
                      <span className="value">30.000</span>
                      <span style={{textDecorationLine: 'underline'}}>đ</span>
                    </div>
                    <div className="type-ship" style={{marginLeft: '17px', color: 'orange'}}>Siêu tốc</div>
                    <div className="time-ship" style={{marginLeft: '17px', color: '#00a4bf'}}>Giao hàng trong vòng 2 ngày</div>
                  </div>
                </div>
              </div>
              <div className="col-product-price">
                <div className="price">
                  <h3 style={{color: 'orange'}}>{product?.quantity*product?.price}<span style={{textDecorationLine: 'underline', marginLeft: '2px'}}>đ</span></h3>
                </div>
              </div>
            </div>
          </div>
        
          </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
    cart:state.cart,
  };
};




export default connect(mapStateToProps,null)(CheckOutDetail);