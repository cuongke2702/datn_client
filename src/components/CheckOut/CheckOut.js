import React, { Component } from "react";
import { connect } from "react-redux";
import CheckOutAction from "../../pages/CheckOutActionPage/CheckOutAction";
import CheckContent from "./CheckContent";
import CheckOutDetail from "./CheckOutDetail";

class CheckOut extends Component {

  constructor(props) {
    super(props);
    this.state = {
    cartIds:{},
    isLoggedIn:{},
    cart:[]
    };
  }

  render() {
      const {isLoggedIn,match,cart}=this.props
    return (
      <div>
        {/* nội dung */}
        <div className="checkout-container ">
          <div className="col-left col-lg-8 col-md-8 col-12">
            <CheckContent isLoggedIn={isLoggedIn} />
            <CheckOutDetail match={match} />
          </div>
          <CheckOutAction match={match} cart={cart} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn : state.users,
    cart:state.cart,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetProduct: (productId) => {
     // dispatch(actGetProductsRequest(productId));
    },
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(CheckOut);
