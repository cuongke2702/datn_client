import React, { Component } from 'react';
import jwt_decode from "jwt-decode";
import { connect } from 'react-redux';
import { getUserById } from '../../actions';

class CheckContent extends Component {
  
  constructor(props) {
    super(props);
    this.setState({
        userId:'',
        userName:'',
        email:'',
        fullName:'',
        phone:'',
        address:'',
    })
  }


    componentDidMount(){
      const {isLoggedIn}=this.props
        var decoded=null;
        var userId=null;
        if(isLoggedIn!=null){
          const {user}=isLoggedIn;
          decoded=jwt_decode(user.token);
          userId=decoded.id;
          console.log(userId)
        }
      //  this.props.onGetUser(userId)
    }

    componentDidUpdate(prevProps, prevState) {
      if (prevProps.register !== this.props.register) {
        const { register } = this.props;
        console.log(register)
        this.setState({
            userId:register.userId,
            userName:register.userName,
            email:register.email,
            fullName:register.fullName,
            phone:register.phone,
            address:register.address,
        });
      }
      
    }
    render() {
        const {register}=this.props
        console.log(register.userId);
        return (
            <div>
                 <div className="area-address">
            <div style={{color: 'black', fontSize: '150%', marginLeft: '10px', width: '100%', textAlign: 'center'}}>
              Thông tin giao hàng
            </div>
            <div className="customer-infomation-card">
              <div className="name-customer">
                <span className="label">Tên khách hàng:</span><span className="value">{register.fullName}</span>
              </div>
              <div className="address">
                <span className="label">Địa chỉ nhận hàng:</span><span className="value">{register.address}</span>
              </div>
              <div className="address-email">
                <span className="label">Email:</span><span className="value">{register.email}</span>
              </div>
              <div className="number-phone">
                <span className="label">Số điện thoại:</span><span className="value">{register.phone}</span>
              </div>
            </div>
            <form>
              <div className="area-address-left">
                <div className="mod-input field-name">
                  <div className="label">Tên</div>
                  <div className="field-input">
                    <input type="text" name="txt-name" placeholder="Nhập họ và tên" data-meta="field" />
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
                <div className="mod-input field-name">
                  <div className="label">Điện thoại</div>
                  <div className="field-input">
                    <input type="text" name="txt-phone" placeholder="Nhập số điện thoại" data-meta="field" />
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
              </div>
              <div className="area-address-right">
                <div className="mod-input field-name">
                  <div className="label">Địa chỉ nhận hàng</div>
                  <div className="field-input">
                    <input type="text" name="txt-address" placeholder="vui lòng nhập địa chỉ của bạn" data-meta="field" />
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
                <div className="mod-input field-name">
                  <div className="label">Tỉnh/Thành phố</div>
                  <input type="hidden" name="txt-tinh" placeholder="Nhập họ và tên" data-meta="field" />
                  <div className="field-select">
                    <div className="select-area area-enable">					
                      <span className="selected-address">tỉnh/thành phố</span>
                      <span className="icon-next-select float-right"><i className="fa fa-angle-down" /></span>
                    </div>
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
                <div className="mod-input field-name">
                  <div className="label">Quận/Huyện</div>
                  <input type="hidden" name="txt-huyen" placeholder="Nhập họ và tên" data-meta="field" />
                  <div className="field-select">
                    <div className="select-area area-disable">					
                      <span className="selected-address">quận/huyện</span>
                      <span className="icon-next-select float-right"><i className="fa fa-angle-down" /></span>
                    </div>
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
                <div className="mod-input field-name">
                  <div className="label">Phường/Xã</div>
                  <input type="hidden" name="txt-xa" placeholder="Nhập họ và tên" data-meta="field" />
                  <div className="field-select">
                    <div className="select-area area-disable">					
                      <span className="selected-address">phường/xã</span>
                      <span className="icon-next-select float-right"><i className="fa fa-angle-down" /></span>
                    </div>
                  </div>
                  <span className="warn">*thông tin bắt buộc*</span>
                </div>
                <div className="btn-save-address">
                  <button type="submit" className="btn-summit">LƯU</button>
                </div>
              </div>
            </form>
          </div>
         
            </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
    register:state.register
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onGetUser: (userId) => {
      dispatch(getUserById(userId));
    },
  };
};


export default connect(mapStateToProps,mapDispatchToProps)(CheckContent);