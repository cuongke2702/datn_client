import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addUserFetchAPI } from '../../actions';
import { Redirect } from 'react-router-dom';

class Register extends Component {

    constructor(props) {
        super(props);
        this.state={
                userName:'',
                password:'',
                email:'',
                fullName:'',
                phone:'',
                address:'',
        };
      }
      componentDidMount(){

      }
      handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value,
        });
      };

      handleSubmit  = async (event) => {
          event.preventDefault()
        var user = {
            userName: this.state.userName,
            password:this.state.password,
            email:this.state.email,
            fullName:this.state.fullName,
            phone:this.state.phone,
            address:this.state.address
        }
        const result = await this.props.onAddUser(user);
    }
      
    render() {

        // const {register} =this.props
        // var data=null;
        // if(register.data!==null){
        //     return <Redirect to="/home" />;
        // }
        return (
            <div id="login">
                <div className="container">
                    <div id="login-row" className="row justify-content-center align-items-center">
                        <div id="login-column" className="col-md-6">
                            <div id="register-box" className="col-md-12">
                                <form id="login-form" className="form" onSubmit={this.handleSubmit}>
                                    <h3 className="text-center text-info">Register</h3>
                                    <div className="form-group">
                                        <label htmlFor="username" className="text-info">Full Name:</label><br />
                                        <input type="text" name="fullName" onChange={this.handleChange} id="fullName" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="username" className="text-info">Phone:</label><br />
                                        <input type="text" name="phone" id="phone" onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="username" className="text-info">Address:</label><br />
                                        <input type="text" name="address" id="address" onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="username" className="text-info">email:</label><br />
                                        <input type="text" name="email" id="email" onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="username" className="text-info">Username:</label><br />
                                        <input type="text" name="userName" id="userName" onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password" className="text-info">Password:</label><br />
                                        <input type="text" name="password" id="password" onChange={this.handleChange} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <input type="submit" name="submit" className="btn btn-info btn-md" defaultValue="submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        register : state .register
    };
  };

const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddUser : (user) =>{
            dispatch(addUserFetchAPI(user))
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Register);
