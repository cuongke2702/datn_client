import React, { Component } from 'react';
import ShopCarrousel from '../Carousel/Shop';

class ProductCenter extends Component {
    render() {
        return (
           <div>
                 {/* trung tâm mua sắm */}
      <div className="vcf-mall-container" id="section-2">
        <div className="title">
          <span style={{marginRight: '5px'}}>--</span><span />Trung tâm mua sắm<span style={{marginLeft: '5px'}}>--</span>
        </div>
        <div className="content mx-auto">
          <div className="mall-slide-carousel">
            <ShopCarrousel/>
          </div>
        </div>
      </div>
             </div>
        );
    }
}

export default ProductCenter;
