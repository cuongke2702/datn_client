import React, { Component } from "react";
import ProductStoreList from "../ProductList/ProductStoreList";

class ProductStore extends Component {
  render() {
    return (
      <div>
        {/* sản phẩm dành riêng cho bạn */}
        <div className="list-result-product" id="section-5">
          <div className="title">
            <span style={{ marginRight: "5px" }}>--</span>
            <span />
            Dành riêng cho bạn<span style={{ marginLeft: "5px" }}>--</span>
          </div>
          <div className="content mx-auto">
            
           <ProductStoreList/>
         
        
          </div>
        </div>
       
      </div>
    );
  }
}

export default ProductStore;
