import React, { Component } from "react";
import ProductList from "../ProductList/ProductList";

class ProductSale extends Component {
  render() {
    return (
      <div>
        {/* sản phẩm ưu đãi khuyến mãi */}
        <div className="list-result-product" id="section-4">
          <div className="title">
            <span style={{ marginRight: "5px" }}>--</span>
            <span />
            Sản phẩm siêu giảm giá<span style={{ marginLeft: "5px" }}>--</span>
          </div>
          <div className="content mx-auto">
               <ProductList/> 
               <ProductList/> 
               <ProductList/> 
               <ProductList/> 
               <ProductList/> 
               <ProductList/> 
         
          </div>
        </div>
      </div>
    );
  }
}

export default ProductSale;
