import React, { Component } from 'react';
import PopularCarrousel from '../Carousel/Popular';

class ProductItem extends Component {

  render() {

    return (
      <div>
        {/* tab-list */}
        <div className="dropdown-sub-menu" dropmenupoisition="ab">
          <ul className="vcf-list dropdown-sub-menu-level-1">
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={1}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
                <li className="vcf-item item-dropdown-sub-menu-level-2" ishover="false">
                  <h6>danh mucj</h6>
                  <ul className="vcf-list dropdown-sub-menu-level-3" />
                </li>
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={2}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={3}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={4}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={5}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={6}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={7}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={8}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
            <li className="vcf-item item-dropdown-sub-menu-level-1" ishover="false" iditem={9}>
              <h6>điện tử gia dụng</h6>
              <ul className="vcf-list dropdown-sub-menu-level-2">
              </ul>
            </li>
          </ul>
        </div>
        {/* slide */}
        <div className="img-slide">
          <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
              <li data-target="#carouselExampleIndicators" data-slide-to={1} />
              <li data-target="#carouselExampleIndicators" data-slide-to={2} />
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img src="../../images/p1.jpg" className="d-block w-100" alt="..." />
              </div>
              <div className="carousel-item">
                <img src="../../images/p2.jpg" className="d-block w-100" alt="..." />
              </div>
              <div className="carousel-item">
                <img src="../../images/p3.jpg" className="d-block w-100" alt="..." />
              </div>
            </div>
          </div>
        </div>

        <div className="services ">
          <div className="services-content mx-auto ">
            <a href="#">
              <div className="card-service" title="Hàng chính hãng">
                <span className="link-card">
                  <span className="card-icon"><i className="fa fa-shopping-bag" /></span>
                  <span className="title-card">Chính hãng</span>
                  <span className="arrow-link">
                    <i className="fa fa-angle-right" />
                  </span>
                </span>
              </div>
            </a><a href="#">
            </a><a href="#">
              <div className="card-service" title="Đánh giá cao">
                <span className="link-card">
                  <span className="card-icon "><i className="fa fa-star" /></span>
                  <span className="title-card">Đánh giá cao</span>
                  <span className="arrow-link">
                    <i className="fa fa-angle-right" />
                  </span>
                </span>
              </div>
            </a>
            <a href="#">
              <div className="card-service" title="Mã giảm giá">
                <span className="link-card">
                  <span className="card-icon"><i className="fa fa-bookmark" /></span>
                  <span className="title-card">Mã giảm giá</span>
                  <span className="arrow-link">
                    <i className="fa fa-angle-right" />
                  </span>
                </span>
              </div>
            </a>
            <a href="#">
              <div className="card-service" title="Ưu đãi">
                <span className="link-card">
                  <span className="card-icon"><i className="fa fa-heart" /></span>
                  <span className="title-card">Ưu đãi</span>
                  <span className="arrow-link">
                    <i className="fa fa-angle-right" />
                  </span>
                </span>
              </div>
            </a>
          </div>
        </div>


        {/* sản phẩm tìm kiếm phổ biến*/}
        <div className="popular-search-product" id="section-1">
          <div className="title">
            <span style={{ marginRight: '5px' }}>--</span><span />Tìm kiếm phổ biến<span style={{ marginLeft: '5px' }}>--</span>
          </div>
          <div className="content mx-auto">
            <PopularCarrousel/>
          </div>
        </div>

      </div>
    );
  }
}

export default ProductItem;
