import "./App.css";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import Menu from "./components/Menu/Menu";
import routes from "./routers";
import { Component } from "react";
import Footer from "./pages/Footer/Footer";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Menu />

          {this.showContentMenus(routes)}
          <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
        </div>
      </Router>
    );
  }

  showContentMenus = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        );
      });
    }
    return <Switch>{result}</Switch>;
  };
}

export default App;
