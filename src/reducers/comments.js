import * as Types from '../constants/ActionTypes';

var initialState = [];

const comments = (state= initialState, action )=>{
    switch (action.type) {
        case Types.GET_COMMENT:
            state = action.comments
            return [...state]
        case Types.ADD_COMMENT:
            state = [...state, action.comment]
            return [...state]
        default:
            return [...state]
    };
};

export default comments;