import * as Types from '../constants/ActionTypes';

const user = JSON.parse(localStorage.getItem("user"));

var initialState = user
? { isLoggedIn: true, user }
: { isLoggedIn: false, user: null };

const users = (state = initialState, action) => {
    switch(action.type){
        case Types.LOGIN:
            console.log(action.user)
            return {
                ...state,
                isLoggedIn: true,
                user:action.user
              };
        case Types.LOGOUT:
            state.isLoggedIn.isLoggedIn= false;
            state.isLoggedIn.user=null;
            console.log(state);
            return {
                ...state,
                };
        default:
            return state;
    }
}

export default users;
