import { combineReducers } from 'redux';
import products from './products';
import itemEditing from './itemEditing';
import cart from './cart';
import comments from './comments';
import register from './register';
import users from './users';

const appReducers = combineReducers({
    products,
    itemEditing,
    cart,
    comments,
    register,
    users,
});

export default appReducers;