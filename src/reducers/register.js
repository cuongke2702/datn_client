import * as Types from '../constants/ActionTypes';

var initialState = {};

const register = (state = initialState, action) => {
    switch(action.type){
        case Types.REGISTER_USER:
            return action.user;
        case Types.GET_USER:
            return action.user;
        default:
            return state;
    }
}

export default register;
