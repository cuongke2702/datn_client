import * as Types from "../constants/ActionTypes";
var data = JSON.parse(localStorage.getItem("cart"));
var initialState = data ? data : [];

const cart = (state = initialState, action) => {
  var { product} = action;
 // var index = -1; // Không tìm thấy => index = -1

  switch (action.type) {
    // case Types.ADD_TO_CART:
    //   index = findProductInCart(state, product);
    //   if (index !== -1) {
    //     state[index].quantity += quantity;
    //   } else {
    //     state.push({ product,quantity,});
    //   }
    //  // localStorage.setItem("CART", JSON.stringify(state));
    //   return [...state];
    case Types.ADD_TO_CART_ITEM:
      var s=JSON.parse(localStorage.getItem("cart"));
      const isExistedProduct = state.find(item => item.productId.toString() === action.product.productId.toString() && item.userId.toString() === action.product.userId.toString())

      if (isExistedProduct) {
        state = state.map(each => {
          if (each.productId.toString() === action.product.productId.toString() && each.userId.toString() === action.product.userId.toString()) {
            return {
              ...each,
              quantity: Number(each.quantity) + Number(action.product.quantity)
            }
          }
          return each
        })
      }

      if (!isExistedProduct) {
        state.push(action.product)
      }
      localStorage.setItem("cart", JSON.stringify(state));
      console.log(state.length);
      console.log(s.length);
      return [...state]

      case Types.THANH_TOAN:
        const newStateValue = []

        state.forEach(each => {
          if (each.productId.toString() !== action.product.productId.toString() || each.userId.toString() !== action.product.userId.toString()) {
            newStateValue.push(each)
          }
        })

        return [...newStateValue]

    default:
      return [...state];
  }
};



export default cart;
