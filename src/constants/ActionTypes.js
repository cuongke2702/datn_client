export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const ADD_STAFF = 'ADD_STAFF';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const DELETE_STAFF = 'DELETE_STAFF';
export const GET_PRODUCT = 'GET_PRODUCT';


export const ADD_TO_CART = 'ADD_TO_CART';

export const ADD_TO_CART_ITEM = 'ADD_TO_CART_ITEM';

export const GET_COMMENT = 'GET_COMMENT';

export const ADD_COMMENT = 'ADD_COMMENT';

export const REGISTER_USER = 'REGISTER_USER';

export const LOGIN= "LOGIN_USER"

export const LOGOUT= "LOGOUT_USER"

export const GET_USER= "GET_USER"

export const THANH_TOAN= "THANH_TOAN"

export const GET_PRODUCT_CART= "GET_PRODUCT_CART"